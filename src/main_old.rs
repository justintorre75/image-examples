//! An example of generating julia fractals.
extern crate image;
extern crate num_complex;
/*
ffmpeg -framerate 1 -pattern_type glob -i '*.png' \                        
  -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4

  ffmpeg -start_number 0 -i inga%d.png -vcodec mpeg4 test.mp4
  -framerate 60 
*/

type Rgba<T> = image::Rgba<T>; 
type RgbaImage = image::ImageBuffer<Rgba<u8>, Vec<u8>>;

// image::ImageBuffer<image::Rgb<u8>, std::vec::Vec<u8>>

fn drawball<P: image::Pixel, Container>(
    background: image::ImageBuffer<P, Container>,
    size: u8,
    position: (u8, u8)
) -> image::ImageBuffer<P, Container> {
    // r^2 = (x - h)^2 + (y - k)^2
    let h = position[0];
    let k = position[1]

    return background;
}

fn get_fractal(scale: f32) -> RgbaImage {
    let imgx = 800;
    let imgy = 800;

    // let scalex = scale / imgx as f32;
    // let scaley = scale / imgy as f32;

    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf = image::ImageBuffer::new(imgx, imgy);

    // Iterate over the coordinates and pixels of the image
    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let r = (scale * x as f32) as u8;
        let b = (scale * y as f32) as u8;
        *pixel = image::Rgb([r, 50, b]);
    }

    // // A redundant loop to demonstrate reading image data
    // for x in 0..imgx {
    //     for y in 0..imgy {
    //         let cx = y as f32 * scalex - 1.5;
    //         let cy = x as f32 * scaley - 1.5;

    //         let c = num_complex::Complex::new(-0.4, 0.6);
    //         let mut z = num_complex::Complex::new(cx, cy);

    //         let mut i = 0;
    //         while i < 255 && z.norm() <= 2.0 {
    //             z = z * z + c;
    //             i += 1;
    //         }

    //         let pixel = imgbuf.get_pixel_mut(x, y);
    //         let image::Rgb(data) = *pixel;
    //         *pixel = image::Rgb([data[0], i as u8, data[2]]);
    //     }
    // }
    return image::DynamicImage::ImageRgb8(drawball(imgbuf, 1, (1,2))).to_rgba();
}


fn main() {
    // for (index, value) in (300..1000).enumerate() {
    //     image::Frame::new(get_fractal(3.0));
    // }
    get_fractal(1.0).save("test.png").unwrap();



    // Save the image as “fractal.png”, the format is deduced from the path
    // for (index, value) in (300..1000).enumerate() {
    //     println!("{}", value as f32 / 10.0);
    //     get_fractal(value as f32 /1000.0).save(format!("inga{number}.png", number=index)).unwrap();
    // }
}